#!/bin/sh

python3 mtgscraper.py

cd mtg-aggregatedeck

./mtg-aggregatedeck --batch --decks "input deck directory"  --out "output directory"
